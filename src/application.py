from database import PeeweeMiddleware
import home
import models
import vk
import web

from falcon import API


def init():
    models.init()
    api = API(media_type='text/plain', middleware=[PeeweeMiddleware()])
    api.add_route('/', home.WebHook())
    api.add_route('/~vk_hook', vk.WebHook())
    api.add_route('/{hook_id}', web.WebHook())
    return api


application = init()
