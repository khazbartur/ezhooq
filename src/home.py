import os

from falcon import HTTPSeeOther

HOME_URL = os.environ['HOME_URL']


class WebHook:
    def on_get(self, req, resp):
        raise HTTPSeeOther(HOME_URL)
