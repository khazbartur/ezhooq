USAGE = 'Usage: /hook [path] [label]'

HOOK_ID_INVALID = 'Path must only contain 3+ letters, numbers, dots, hyphens or underscores'
HOOK_ID_TAKEN = '{} is already taken, please come up with something else'
HOOK_CREATED = 'Hook {} successfully created\n{}'
HOOK_TRIGGERED = 'New event on hook {}\n{} {}\n{}'

STATUS_OK = '{"status": "ok"}'
STATUS_NOT_FOUND = '{{"status": "not found", "hook_id": "{}"}}'
