from peewee import Model
from playhouse import db_url
import os

DBMS = os.environ['DB_DBMS']
HOST = os.environ['DB_HOST']
PORT = os.environ['DB_PORT']
USER = os.environ['DB_USER']
PSWD = os.environ['DB_PSWD']
NAME = os.environ['DB_NAME']

db = db_url.connect(f'{DBMS}://{USER}:{PSWD}@{HOST}:{PORT}/{NAME}')


class Entity(Model):
	class Meta:
		database = db


class PeeweeMiddleware:
	def process_resource(self, req, resp, resource, params):
		db.connect()

	def process_response(self, req, resp, resource, req_succeeded):
		if not db.is_closed():
			db.close()
