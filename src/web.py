from falcon import HTTPNotFound

from models import Hook
import vk
import strings


class WebHook:
    def on_post(self, req, resp, hook_id):
        hook = Hook.get_or_none(hook_id=hook_id)
        if hook is None:
            resp.body = strings.STATUS_NOT_FOUND.format(hook_id)
            raise HTTPNotFound

        chunk_size = 4000
        first = True
        while bytes_chunk := req.stream.read(chunk_size):
            content_chunk = bytes_chunk.decode(errors='replace')
            vk.send_message(hook.owner.peer_id,
                strings.HOOK_TRIGGERED.format(
                    repr(hook.label),
                    req.method, req.relative_uri,
                    content_chunk
                ) if first else content_chunk
            )
            first = False

        resp.body = strings.STATUS_OK

    on_get = on_post
