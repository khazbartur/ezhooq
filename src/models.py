from peewee import BigIntegerField, BooleanField, CharField, ForeignKeyField

from database import db, Entity


class Peer(Entity):
    peer_id = BigIntegerField(primary_key=True)


class Hook(Entity):
    hook_id   = CharField(primary_key=True)
    label     = CharField()
    is_active = BooleanField(default=True)
    owner     = ForeignKeyField(Peer)


def init():
    db.connect()
    db.create_tables((Peer, Hook))
    db.close()
