import os
import random

import peewee
import falcon
import requests

from models import Peer, Hook
import strings

API = 'https://api.vk.com/method/'
VERSION = '5.89'

CONFIRM = os.environ['VK_CONFIRM']
SECRET = os.environ['VK_SECRET']
TOKEN = os.environ['VK_TOKEN']

HOST = os.environ['HOST']

ALPHANUM = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
SPECIAL  = '._-'


def validate(hook_id):
    return (
        3 <= len(hook_id) and set(hook_id).issubset(ALPHANUM + SPECIAL)
    )


def send_message(peer_id, text):
    requests.post(API + 'messages.send', {
        'peer_id': peer_id,
        'message': text,
        'access_token': TOKEN,
        'v': VERSION,
    })


def on_message(message):
    peer_id = message['peer_id']
    text = message['text']

    peer, created = Peer.get_or_create(peer_id=peer_id)

    cmd, *rest = text.split()
    cmd = cmd.lower()

    if cmd == '/hook':
        if len(rest) == 0:
            hook_id = ''.join(random.choices(ALPHANUM, k=6))
        else:
            hook_id = rest[0]

        if len(rest) > 1:
            label = ' '.join(rest[1:])
        else:
            label = hook_id

        if not validate(hook_id):
            send_message(peer_id, strings.HOOK_ID_INVALID)
            return

        try:
            Hook.create(hook_id=hook_id, label=label, owner=peer_id)
        except peewee.IntegrityError:
            send_message(peer_id, strings.HOOK_ID_TAKEN.format(repr(hook_id)))
            return

        send_message(peer_id, strings.HOOK_CREATED.format(
            repr(label), HOST + hook_id))

    else:
        send_message(peer_id, strings.USAGE)


class WebHook:
    def on_post(self, req, resp):
        event = req.media

        if not (event and 'type' in event):
            raise falcon.HTTPBadRequest

        print(event)

        if event.get('secret') != SECRET:
            raise falcon.HTTPForbidden

        if event.get('type') == 'confirmation':
            resp.body = CONFIRM

        elif event.get('type') == 'message_new':
            message = event['object']
            action = message.get('action')

            if not action:
                on_message(message)

            resp.body = 'ok'

        else:
            raise falcon.HTTPNotImplemented
